# ClientsPreferencesApp

This project is the client of microservice client project that runs in http://localhost:8181/. This application is an small project for getting a list of clients and displays relevant information of client as well a prefered plan.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.