import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
 
let client_service = "http://localhost:8181/api/v1/clientes/";
 
export interface ClientInterface {
  cliente_id : ClienteidInterface | any;
  primer_nombre: string | any;
  segundo_nombre: string | any;
  primer_apellido: string | any;
  segundo_apellido: string | any;
  direccion_residencia: string | any;
  numero_celular: number | any;
  email: string | any;
  fecha_nacimiento: string | any;
  plan_preferido: number | any;

}

export interface ClienteidInterface {
  tipo_Identificacion: string;
  numero_Identificacion: number;
}


 
@Injectable({
  providedIn: 'root'
})
export class ClientService {
 
  constructor(private http : HttpClient) { }
 
  loadClients() {
      return this.http.get<Array<ClientInterface>>(client_service);
  }
 
  createClient(client: ClientInterface) {
    return this.http.post<ClientInterface>(client_service, client);
  }
 
  updateClient(client: ClientInterface) {
    return this.http.put<ClientInterface>(client_service, client);
  }
 
  deleteClient(id:ClienteidInterface) {
    return this.http.delete<String>(client_service + id, { responseType: 'text' as 'json'}
    );
  }
}