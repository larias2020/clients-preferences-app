import { Component, OnInit } from '@angular/core';
import { ClientService, ClientInterface, ClienteidInterface } from '../../client/service/client.service';
import { NgbModal, NgbModalRef, NgbModalOptions, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
 
@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

  _clients: Array<ClientInterface> = [];
  _message: String = "";
  _client!: ClientInterface | any;
  _client_temp!: ClientInterface | any;
  myDate: Date = new Date();
  //_dateClient!: NgbDateStruct;
  model!:NgbDateStruct

  modalReference!: NgbModalRef;
  modalOption: NgbModalOptions = {};
 
  constructor(private clientService: ClientService,
    private modalService: NgbModal) { }

 
  deleteClient(id: ClienteidInterface) {
    this.clientService.deleteClient(id).subscribe((msg: String) => this._message = msg);
    this._clients.splice(this._clients.findIndex(s => s.cliente_id === id), 1);
  }
 
  updateClient(id: ClienteidInterface) {
   this.clientService.updateClient(this._client).subscribe((client: any) => this._client = client);
  }
 
  addClient() {
   this.clientService.createClient(this._client).subscribe((client: any) => {
      this._client = client;
      this._clients.push(this._client);
    });
  }
 
  createUpdate() {
    this._client_temp = this._clients.find(s => s.cliente_id === this._client.ClienteidInterface);
    if( this._client_temp){
      this.myDate.setMonth(this.model.month);
      this.myDate.setFullYear(this.model.year);
      this.myDate.setDate(this.model.day);
      this._client.fecha_nacimiento = this.myDate;
      this.addClient();
    } else {
      this.updateClient(this._client.ClienteidInterface);
    }
    this.modalReference.close();
  }
 
  ngOnInit(): void {
    this.clientService.loadClients().subscribe((clients: any[]) => this._clients = clients);
    this._message = "";
  }
 
  open(id : ClienteidInterface | any , content: any) {
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;
    this.modalReference = this.modalService.open(content, this.modalOption);
    if(id === null ) {
      this._client = {
        cliente_id : {tipo_Identificacion: null, numero_Identificacion:0}, 
        primer_nombre: null,
        segundo_nombre: null, primer_apellido: null,
        segundo_apellido: null, direccion_residencia:null,
        numero_celular : 0, email: null,fecha_nacimiento:null, 
        plan_preferido:0
        };
    } else {
      this._client = this._clients.find(s => s.cliente_id === id);
    }
  }
  
}
