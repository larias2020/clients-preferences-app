import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgbDateStruct, NgbCalendar} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-datepicker-basic',
  templateUrl: './datepicker-basic.html'
})
export class NgbdDatepickerBasic {

    date!: { year: number; month: number; };

    @Input() _date!: NgbDateStruct;
 
    @Output() _dateChanged: EventEmitter<NgbDateStruct> =   new EventEmitter();

  constructor(private calendar: NgbCalendar) {
  }

  selectToday() {
    this._date = this.calendar.getToday();
  }
}